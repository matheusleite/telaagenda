/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import agendaoo.controle.ControlePessoa;
import agendaoo.modelo.Pessoa;
/**
 *
 * @author User
 */
public class ControlePessoaTest {
    
    public ControlePessoaTest() {
    }
    private Pessoa pessoaTest;
    private ControlePessoa controleTest;
	
    @Before
    public void setUp() throws Exception {
        
    }
    @Test
	public void testAdicionar() {
		pessoaTest = new Pessoa();
		pessoaTest.setNome("Matheus");
		controleTest = new ControlePessoa();
		controleTest.adicionar(pessoaTest);
		assertEquals(controleTest.adicionar(pessoaTest), "Pessoa adicionada com Sucesso!");
	}
	public void testRemover() {
		pessoaTest = new Pessoa();
		pessoaTest.setNome("Matheus");
		controleTest = new ControlePessoa();
		controleTest.remover(pessoaTest);
		assertEquals(controleTest.remover(pessoaTest), "Pessoa removida com Sucesso!");
	}
	public void testPesquisarNome() {
		pessoaTest = new Pessoa();
		pessoaTest.setNome("Matheus");
		controleTest = new ControlePessoa();
		controleTest.pesquisarNome("Matheus");
		assertEquals(controleTest.pesquisarNome("Matheus"), "Matheus");
	}
	public void testPesquisarTelefone() {
		pessoaTest = new Pessoa();
		pessoaTest.setTelefone("81721416");
		controleTest = new ControlePessoa();
		controleTest.pesquisarTelefone("81721416");
		assertEquals(controleTest.pesquisarTelefone("81721416"), "81721416");
	}

}
    
    

    
