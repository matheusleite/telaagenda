/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import agendaoo.modelo.Pessoa;
/**
 *
 * @author User
 */
public class PessoaTest {
    
    private Pessoa pessoaTest;
    
    public PessoaTest() {
    }
   
    
    @Before
    public void setUp() throws Exception{
        
    }
    
    @Test
	public void testNome() {
		pessoaTest = new Pessoa();
		pessoaTest.setNome("Matheus");
		
		assertEquals(pessoaTest.getNome(), "Matheus");
	}
	
	
	@Test
	public void testTelefone() {
		pessoaTest = new Pessoa();
		pessoaTest.setTelefone("8172-1416");
		
		assertEquals(pessoaTest.getTelefone(), "8172-1416");
	}
	
	@Test
	public void testIdade() {
		pessoaTest = new Pessoa();
		pessoaTest.setIdade("18");
		
		assertEquals(pessoaTest.getIdade(), "18");
	}
	
	@Test
	public void testEndereco() {
		pessoaTest = new Pessoa();
		pessoaTest.setEndereco("Riacho 1");
		
		assertEquals(pessoaTest.getEndereco(),"Riacho 1");
	}
	
	@Test
	public void testRg() {
		pessoaTest = new Pessoa();
		pessoaTest.setRg("2736244");
		
		assertEquals(pessoaTest.getRg(), "2736244");
	}
	
	@Test
	public void testCpf() {
		pessoaTest = new Pessoa();
		pessoaTest.setCpf("05176984121");
		
		assertEquals(pessoaTest.getCpf(), "05176984121");
	}
	
	@Test
	public void testSexo() {
		pessoaTest = new Pessoa();
		pessoaTest.setSexo('F');
		
		assertEquals(pessoaTest.getSexo(), 'F');
	}
	
	@Test
	public void testEmail() {
		pessoaTest = new Pessoa();
		pessoaTest.setEmail("a@a.com");
		
		assertEquals(pessoaTest.getEmail(), "a@a.com");
	}
}