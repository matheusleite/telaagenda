package agendaoo.modelo;

import java.io.*;
import agendaoo.controle.ControlePessoa;

public class CadastroPessoa{


    public static void main(String[] args) throws IOException{
        //burocracia para leitura de teclado
        InputStream entradaSistema = System.in;
        InputStreamReader leitor = new InputStreamReader(entradaSistema);
        BufferedReader leitorEntrada = new BufferedReader(leitor);
        String entradaTeclado;
        char menuOpcao;
        //instanciando objetos do sistema
        ControlePessoa umControle = new ControlePessoa();
        Pessoa umContato = new Pessoa();
        String nomeContato;

        //interagindo com usuário
        do {
            System.out.println("=======Menu=======");
            System.out.println("1 - Adicionar Contato");
            System.out.println("2 - Remover Contato");
            System.out.println("3 - Buscar Contato por nome");
            System.out.println("4 - Buscar Contato por telefone");
            System.out.println("0 - Sair");


            System.out.println("Insira a opcao: ");
            entradaTeclado = leitorEntrada.readLine();
            menuOpcao = entradaTeclado.charAt(0);

            switch (menuOpcao) {
                case '1' :
                    System.out.println("Digite o nome da Pessoa:");
                    entradaTeclado = leitorEntrada.readLine();
                    String umNome = entradaTeclado;

                    System.out.println("Digite o telefone da Pessoa:");
                    entradaTeclado = leitorEntrada.readLine();
                    String umTelefone = entradaTeclado;

                    System.out.println("Digite a idade da Pessoa: ");
                    entradaTeclado = leitorEntrada.readLine();
                    String umaIdade = entradaTeclado;

                    System.out.println("Digite o sexo da Pessoa (M/F): ");
                    entradaTeclado = leitorEntrada.readLine();
                    char umSexo = entradaTeclado.charAt(0);

                    System.out.println("Digite o email da Pessoa: ");
                    entradaTeclado = leitorEntrada.readLine();
                    String umEmail = entradaTeclado;

                    System.out.println("Digite o hangout da Pessoa: ");
                    entradaTeclado = leitorEntrada.readLine();
                    String umHangout = entradaTeclado;

                    System.out.println("Digite o endereço da Pessoa: ");
                    entradaTeclado = leitorEntrada.readLine();
                    String umEndereco = entradaTeclado;

                    System.out.println("Digite o RG da Pessoa: ");
                    entradaTeclado = leitorEntrada.readLine();
                    String umRg = entradaTeclado;

                    System.out.println("Digite o Cpf da Pessoa: ");
                    entradaTeclado = leitorEntrada.readLine();
                    String umCpf = entradaTeclado;

                    //adicionando uma pessoa na lista de pessoas do sistema
                    Pessoa umaPessoa = new Pessoa(umNome, umTelefone);

                    //Setando a entrada do usuário
                    umaPessoa.setIdade(umaIdade);
                    umaPessoa.setSexo(umSexo);
                    umaPessoa.setEmail(umEmail);
                    umaPessoa.setHangout(umHangout);
                    umaPessoa.setEndereco(umEndereco);
                    umaPessoa.setRg(umRg);
                    umaPessoa.setCpf(umCpf);

                    String mensagem = umControle.adicionar(umaPessoa);

                    //conferindo saída
                    System.out.println("=================================");
                    System.out.println(mensagem);
                    System.out.println("=)");
                break;
                case '2' :
                    umControle.exibirContatos();
                    System.out.println("\nDigite o nome da Pessoa: ");
                    entradaTeclado = leitorEntrada.readLine();
                    nomeContato = entradaTeclado;

                    umContato = umControle.pesquisarNome(nomeContato);
                    umControle.remover(umContato);
                break;
                case '3' : 
                    System.out.println("Digite o nome da Pessoa: ");
                    entradaTeclado = leitorEntrada.readLine();
                    nomeContato = entradaTeclado;

                    umContato = umControle.pesquisarNome(nomeContato);
                    System.out.println("Nome: " + umContato.getNome());
                    System.out.println("Telefone: " + umContato.getTelefone());
                    System.out.println("Idade: " + umContato.getIdade());
                    System.out.println("Sexo: " + umContato.getSexo());
                    System.out.println("Endereco: " + umContato.getEndereco());
                    System.out.println("Rg: " + umContato.getRg());
                    System.out.println("Cpf: " + umContato.getCpf());
                    System.out.println("Hangout: " + umContato.getHangout());
                    System.out.println("Email: " + umContato.getEmail());
                break;
                case '4' :
                    System.out.println("Digite o telefone da Pessoa: ");
                    entradaTeclado = leitorEntrada.readLine();
                    String telefoneContato = entradaTeclado;

                    umContato = umControle.pesquisarTelefone(telefoneContato);
                    System.out.println("Nome: " + umContato.getNome());
                    System.out.println("Telefone: " + umContato.getTelefone());
                    System.out.println("Idade: " + umContato.getIdade());
                    System.out.println("Sexo: " + umContato.getSexo());
                    System.out.println("Endereco: " + umContato.getEndereco());
                    System.out.println("Rg: " + umContato.getRg());
                    System.out.println("Cpf: " + umContato.getCpf());
                    System.out.println("Hangout: " + umContato.getHangout());
                    System.out.println("Email: " + umContato.getEmail());
                break;
                case '0' :
                    menuOpcao = '0';
                break;
                default :
                    System.out.println("Opcao incorreta! Tente novamente!");
            }
        } while ( menuOpcao != '0' );
    }

}
